import 'package:flutter/material.dart';
import 'package:flutter_travel_app/widget/most_popular.dart';
import 'package:flutter_travel_app/widget/travel_blog.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: Icon(
              Icons.menu,
              color: Colors.black,
            ),
          ),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 15),
            child: Text('Travel Blog',
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),),
          ),
          Expanded(
            flex: 2,
            child: TravelBlog()),
          Padding(
            padding: EdgeInsets.all(15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Most Popular',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),),
                Text('View All',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.deepOrange,
                ),),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: MostPopular()),
        ],
      ),
    );
  }
}
