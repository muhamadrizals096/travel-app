import 'package:flutter_travel_app/widget/most_popular.dart';

class Travel {
  String name;
  String location;
  String url;

  Travel(this.name, this.location, this.url);

  static List<Travel> generateTravelBlog() {
    return [
        Travel(
          'Pantai Kuta Bali', 
          'Bali, Indonesia',
          'assets/images/top1.jpg'),
        Travel(
          'Pantai Pangandaran', 
          'Jawa Barat, Indonesia',
          'assets/images/top2.jpg'),
        Travel(
          'Pantai Ujung Genteng', 
          'Jawa Barat, Indonesia',
          'assets/images/top3.jpg'),
        Travel(
          'Pantai Santolo', 
          'Jawa Barat, Indonesia',
          'assets/images/top4.jpg'),
    ];
  }

  static List<Travel> generateMostPopular() {
    return [
        Travel(
          'Ujung Genteng', 
          'Jawa Barat',
          'assets/images/bottom1.jpg'),
        Travel(
          'Parangtritis', 
          'Jawa Tengah',
          'assets/images/bottom2.jpg'),
        Travel(
          'Lombok', 
          'Lombok',
          'assets/images/bottom3.jpg'),
        Travel(
          'Karang Hawu', 
          'Jawa Barat',
          'assets/images/bottom4.jpg'),
    ];
  }
}

